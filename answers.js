// 2
let sumPositives = (arr) => {
    return arr.reduce((a, b) => b>0 ? a + b : a, 0)
}

// 3
let frequencySort = (arr) => {
    let frequencyCount = {};
    arr.forEach(element => {
        frequencyCount[element] = frequencyCount[element] ? frequencyCount[element] + 1 : 1;
    });
    return [...arr].sort((a, b) => {
      return frequencyCount[b] - frequencyCount[a] || a - b
    })
  }

// 4
let findUniqueNumber = (arr) => {
    let [a,b]  = new Set(arr)
    return arr.slice(0,3).reduce( (count,element) => element === a ? count + 1 : count, 0) === 1 ? a : b
}

// 5
let findSubstring = (str) => {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz'
  let Max = ''
  let cursor = ''
  str.split('').forEach((element, i) => {
    if(cursor === '' || alphabet.indexOf(cursor[cursor.length-1]) < alphabet.indexOf(element)){
      cursor += element
      if(cursor.length> Max.length){
        Max = cursor
      }
    }else{
      cursor = ''
    }
  });
  return Max
}